class User < ApplicationRecord
  has_many :posts
  has_many :relacionamentos_on, class_name:  'Relacionamento',
           foreign_key: 'seguidor_id',
           dependent:   :destroy

  has_many :relacionamento_inv, class_name:  'Relacionamento',
           foreign_key: 'seguido_id',
           dependent:   :destroy

  has_many :seguindo, through: :relacionamentos_on, source: :seguido
  has_many :seguidores, through: :relacionamento_inv, source: :seguidor

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  def nome_completo
    return nome + ' ' + sobrenome
  end

  def seguir(usuario)
    seguindo << usuario
  end

  def deixar_de_seguir(usuario)
    seguindo.delete(usuario)
  end

  def seguindo?(usuario)
    seguindo.include?(usuario)
  end


end
