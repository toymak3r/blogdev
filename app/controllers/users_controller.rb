class UsersController < ApplicationController

  def visualizar
    @user = User.find(params[:user_id])
    @posts = Post.where(:user_id => @user.id)
  end

  def index
    @users = User.all
  end
end
