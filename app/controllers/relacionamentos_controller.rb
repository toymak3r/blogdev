class RelacionamentosController < ApplicationController

  # GET /relacionamento/new
  def new
    @relacionamento = Relacionamento.new
  end

  def destroy
    respond_to do |format|
      if current_user.deixar_de_seguir(params[:seguido_id])
        format.html { redirect_to root_path, notice: 'Ok, deixou de seguir.' }
        format.json { render :show, status: :created, location: @relacionamento }
      else
        format.html { redirect_to root_path, error: 'Nao foi possivel deixar de seguir' }
        format.json { render json: @relacionamento.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @relacionamento = Relacionamento.new
    @relacionamento.seguidor_id = current_user.id
    @relacionamento.seguido_id = params[:seguido_id]

    respond_to do |format|
      if @relacionamento.save
        format.html { redirect_to root_path, notice: 'Sucesso na solicitaçao! Novos Posts em sua timeline!' }
        format.json { render :show, status: :created, location: @relacionamento }
      else
        format.html { redirect_to root_path, error: 'Sucesso na solicitaçao! Novos Posts em sua timeline!' }
        format.json { render json: @relacionamento.errors, status: :unprocessable_entity }
      end
    end
  end
end
