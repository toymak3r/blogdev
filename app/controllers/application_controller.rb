class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception

  def home
    @users = User.all
    @user = current_user
    if !current_user.nil?
      @posts = current_user.posts
      @following_posts = Post.where(:user_id => current_user.seguindo ).order(created_at: :desc)
    end

  end

  def configure_permitted_parameters
    added_attrs = [:login, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

end
