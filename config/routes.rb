Rails.application.routes.draw do
  resources :posts
  resources :users
  resources :relacionamentos

## devise controllers for users
  devise_for :user, controllers: {
      # confirmations: 'users/confirmations',
      passwords: 'users/passwords',
      registrations: 'users/registrations',
      sessions: 'users/sessions',
      # unlocks: 'users/unlocks',
  }, skip: [:sessions]

  as :user do
    get 'login' => 'users/sessions#new', as: :new_user_session
    post 'login' => 'users/sessions#create', as: :user_session
    delete 'logout' => 'users/sessions#destroy', as: :destroy_user_session
    get 'register' => 'users/registrations#new'
  end

  get 'user/:user_id', to: 'users#visualizar'

  root to: "application#home"



end
